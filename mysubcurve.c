#include "mysubcurve.h"

  template <typename T>
  inline
  mySubCurve<T>::mySubCurve( GMlib::PCurve<T,3>* c, T s, T e )
  {
    this->_dm = GMlib::GM_DERIVATION_EXPLICIT;

    set(c, s, e, (e+s)/2);

  }

  template <typename T>
  inline
  mySubCurve<T>::mySubCurve( GMlib::PCurve<T,3>* c, T s, T e, T t )
  {
    this->_dm = GMlib::GM_DERIVATION_EXPLICIT;

    set(c, s, e, t);

    GMlib::DVector<GMlib::Vector<T,3> > tr = _c->evaluateParent(t, 0);
    _trans = tr[0];
    this->translate( _trans );

  }


  template <typename T>
  inline
  mySubCurve<T>::mySubCurve( const mySubCurve<T>& copy ) : GMlib::PCurve<T,3>( copy )
  {
    set(copy._c, copy._s, copy._e, copy._t);
    _trans = copy._trans;
  }


  template <typename T>
  mySubCurve<T>::~mySubCurve() {}


  // Virtual functions from PSurf
  //******************************

  template <typename T>
  void mySubCurve<T>::eval( T t, int d, bool /*l*/ )
  {
    this->_p     = _c->evaluateParent(t , d);
    this->_p[0] -= _trans;
  }


  template <typename T>
  T mySubCurve<T>::getStartP()
  {
    return _s;
  }


  template <typename T>
  T mySubCurve<T>::getEndP()
  {
    return _e;
  }


  template <typename T>
  bool mySubCurve<T>::isClosed() const
  {
    return false;
  }


  // Private help functions
  //*************************

  template <typename T>
  inline
  void mySubCurve<T>::set(GMlib::PCurve<T,3>* c, T s, T e, T t)
  {
    _c = c;
    _s = s;
    _t = t;
    _e = e;
  }




