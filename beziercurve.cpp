#include "beziercurve.h"

//#include <scene/render/gmdefaultrenderer.h>
//#include <parametrics/visualizers/gmpcurvevisualizer.h>
//#include <parametrics/visualizers/gmpcurvedefaultvisualizer.h>

#include <cmath>

Beziercurve::Beziercurve(GMlib::PCurve<float,3>* curve,float start, float end,float t, int d){
    _closed = curve->isClosed();
    _curve  = curve;
    _start  = start;
    _end    = end;
    _t      = t ;
    _d      = d;//derivatives
   this->ControlPoint(t,d);

}

Beziercurve::~Beziercurve(){

     delete _curve;
}

void Beziercurve::eval(float t, int d, bool){
    this->_p.setDim(1 + _d);  // ??? the same (1+d) behind line
    float scale = 1.0f/(_end - _start);
    GMlib::DMatrix<float> m = BezierMatrix(_d, t,scale);
    this->_p = m * _controlpoint;
    this->_p.setDim(1 + d);


}

GMlib::DMatrix<float> Beziercurve::BezierMatrix(int d, float t,float scale ){
    GMlib::DMatrix<float> B(d+1,d+1);

    B[d-1][0]= 1 - t;
    B[d-1][1]= t;

    for(int i = d - 2; i >= 0; i--){
        B[i][0] = (1-t) * B[i+1][0];

        for(int j = 1 ; j < d-i ; j++)
            B[i][j] = t * B[i+1][j-1]+(1-t) * B[i+1][j] ;
        B[i][d-i] = t * B[i+1][d-i-1];

    }

    B[d][0] = -scale;
    B[d][1] = scale;

    for(int k = 2; k <= d ; k++){

        double s = k * scale;
        for(int i = d; i> d-k ; i--){
            B[i][k] = s * B[i][k-1] ;

            for(int j = k-1; j>0 ; j--)

                B[i][j] = s *(B[i][j-1] - B[i][j] );
                B[i][0] = -s * B[i][0];
        }
    }


   return B;

}


void Beziercurve::ControlPoint(float t, int d){

    float scale = 1.0f;//(_end - _start);
    float newt =(t-_start)/(_end - _start);

    GMlib::DMatrix<float>  m = this->BezierMatrix(d,newt,scale);
    m.invert();
    GMlib::DVector<GMlib::Vector<float,3> > h = _curve->evaluateParent(t,d);

    _controlpoint = m * h;

    for(int i=0; i<_controlpoint.getDim(); i++)
        _controlpoint[i] -= h[0];
    this->translate(h[0]);   
}

bool Beziercurve::isClosed(){
    return _closed;
}

float Beziercurve::getStartP(){
    return 0.0;
}


float Beziercurve::getEndP(){
    return 1.0;
}


