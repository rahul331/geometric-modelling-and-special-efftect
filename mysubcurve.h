#ifndef MYSUBCURVE_H
#define MYSUBCURVE_H
#include <parametrics/gmpcurve>




template <typename T>
class mySubCurve : public GMlib::PCurve<T,3> {
    GM_SCENEOBJECT(mySubCurve)

public:
    mySubCurve( GMlib::PCurve<T,3>* c, T s, T e);
    mySubCurve( GMlib::PCurve<T,3>* c, T s, T e, T t);
    mySubCurve( const mySubCurve<T>& copy );

    virtual ~mySubCurve();

    // virtual functions from PSurf
    virtual bool            isClosed() const;
    // Local functions
    void                    togglePlot();

protected:

    GMlib::PCurve<T,3>* _c;
    T                   _s;
    T                   _t;
    T                   _e;
    GMlib::Vector<float,3>     _trans;

    // virtual functions from PSurf
    void                eval( T t, int d, bool l = false );
    T                   getEndP();
    T                   getStartP();

private:

    // Local help functions
    void set(GMlib::PCurve<T,3>* c, T s, T e, T t);

}; // END class mySubCurve



#include "mysubcurve.c"
#endif // MYSUBCURVE_H
