
//#include <parametrics/gmpcurve>
#include"knotvector.h"

knotvector::knotvector(int n, float start,float end){


    setDim(n+2);

    float delta =(end-start)/(n-1);

    (*this)[0] = (*this)[1]=start;

    for (int i=1; i<n-1; i++)
        (*this)[i+1] = (*this)[i]+delta;

    (*this)[n] = (*this)[n+1]=end;


}
float knotvector::getStart(){
    return(*this)[1];
}

float knotvector::getEnd(){
    return(*this)[getDim()-2];
}


