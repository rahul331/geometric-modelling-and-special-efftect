#ifndef EBRSCURVE_H
#define EBRSCURVE_H

#include <parametrics/gmpcurve>
#include "knotvector.h"
#include"mysubcurve.h"
#include"curve.h"
#include"beziercurve.h"



template<typename T>
class ebrsCurve: public GMlib::PCurve<T,3>{

    GM_SCENEOBJECT(ebrsCurve )

public:
    ebrsCurve( GMlib::PCurve<T,3>* c, int n );
    ebrsCurve( GMlib::PCurve<T,3>* c, int d, int n);
    ebrsCurve( const ebrsCurve<T>& copy );

    virtual ~ebrsCurve();

    bool   isClosed() const;

protected:

    void    eval(T t, int d, bool l = true );
    T       getEndP();
    T       getStartP();

private:
    void  makeknotvector(GMlib::PCurve<T,3>* c, int n);
    void  generateSubCurve(GMlib::PCurve<T,3>* c, int n);
    void  localBezierCurve(GMlib::PCurve<float,3> *b, int d, int n);

    void  localSimulate(double dt);

    GMlib::DVector<float> B(float t, int d);
    GMlib::DVector<float> kvector;
    GMlib::DVector<GMlib::PCurve<T,3>*> localcurve;


    bool isclosed;
    bool isBezier;
    float  getMap(float t, int index);


};

#include"ebrscurve.c"

#endif // EBRSCURVE_H
