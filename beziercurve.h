#ifndef BEZIERCURVE_H
#define BEZIERCURVE_H

#include <gmParametricsModule>
//#include<parametrics/gmpcurve>


class Beziercurve : public GMlib::PCurve<float,3>{
    GM_SCENEOBJECT(Beziercurve)


public:
        Beziercurve(GMlib::PCurve<float,3>* curve, float start, float end, float t, int d); // t ???
       ~Beziercurve();
        bool isClosed();


protected:
   GMlib::DVector<GMlib::Vector<float,3> > _controlpoint;
   GMlib::PCurve<float,3>*                 _curve;
    float                                  _start;
    float                                  _t;
    float                                  _end;
    bool                                   _closed;
    int                                    _d;


    float getStartP();
    float getEndP();
    void  eval(float t, int d, bool l = true ) ;


    GMlib::DMatrix<float> BezierMatrix( int d, float t, float scale ); // bernsteinHemite property
    void ControlPoint(float t, int d);

private:

};

#endif // BEZIERCURVE_H
