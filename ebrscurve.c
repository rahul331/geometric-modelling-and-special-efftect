#include"ebrscurve.h"
#include "iostream"
#include"math.h"


template<typename T>
ebrsCurve<T>::ebrsCurve(GMlib::PCurve<T,3>* c,int n){
    if (c->isClosed()){
        n++;
        isclosed =true;
    }
    else isclosed =false;

    this->makeknotvector(c,n);
    //this->generateSubCurve(c,n);
    this->localBezierCurve(c, 3, n);
}

template<typename T>
ebrsCurve<T>::ebrsCurve(GMlib::PCurve<T,3>* c,int d,int n){

}

template<typename T>
ebrsCurve<T>::ebrsCurve(const ebrsCurve<T> & copy){

}

template<typename T>
ebrsCurve<T>::~ebrsCurve(){}


template<typename T>
void ebrsCurve<T>::makeknotvector(GMlib::PCurve<T,3>* c, int n){

    T start=c->getParStart();
    T end=c->getParEnd();


    float delta = (end-start)/(n-1);

    kvector.setDim(n+2);

    for(int i=0; i<n; i++){
        kvector[i+1] = start+ i*delta;

    }

    if(c->isClosed()){
        kvector[0] = kvector[1] - (kvector[n] - kvector[n-1]);
        kvector[n+1] = kvector[n] + (kvector[2] - kvector[1]);
    }
    else{
        kvector[0] = kvector[1];
        kvector[n+1] = kvector[n];

    }

}


template<typename T>
void ebrsCurve<T>::generateSubCurve(GMlib::PCurve<T,3>* c, int n){

    localcurve.setDim(n);


    for(int i=0; i<n; i++){

       localcurve[i] = new mySubCurve<T>(c, kvector[i], kvector[i+2], kvector[i+1]);
       localcurve[i]->toggleDefaultVisualizer();
       localcurve[i]->replot(20,1);
       localcurve[i]->setColor(GMlib::GMcolor::Blue);
       //this->insert(localcurve[i]);
       //this->scale(localcurve);
}

    if(c->isClosed()){
        localcurve[n-1] =  localcurve[0];
    }
    else{
        localcurve[n-1] = new mySubCurve<T>(c, kvector[n-1], kvector[n+1], kvector[n]);

    }

}

template<typename T>
void ebrsCurve<T>::localBezierCurve(GMlib::PCurve<float,3> *b,int d,int n){  // d: degree or number of derivative ???

    localcurve.setDim(n);
    for(int i = 1; i < n;i++){
        localcurve[i-1]= new Beziercurve(b,kvector[i-1],kvector[i+1],kvector[i],d);

    }

    if(isClosed())
        localcurve[n-1]= localcurve[0];

    else
        localcurve[n-1]= new Beziercurve(b,kvector[n-1],kvector[n+1],kvector[n],d);       
}

template<typename T>
float  ebrsCurve<T>::getMap(float t,int index ){

    if(isBezier)

        return (t-kvector[index-1])/(kvector[index + 1] - kvector[index-1]);
    else
        return t;

}

template<typename T>
void ebrsCurve<T>::eval( T t, int d, bool /*l*/ ) {//calculating index

    this->_p.setDim( d + 1 );


//find index

    int index=1,i ;

    for(index=1; index< kvector.getDim()-2; index++)
        if(t>=kvector[index] && t< kvector[index+1])break;
    if(index>kvector.getDim()-3)
        index=kvector.getDim()-3;


    GMlib::DVector<float> Bfunc=B((t-kvector[index])/(kvector[index+1]-kvector[index]),d);
    GMlib::PCurve<T,3> *c1 = localcurve[index-1];
    GMlib::PCurve<T,3> *c2 = localcurve[index];


    GMlib::DVector<GMlib::Vector<float,3> > ec1= c1-> evaluateParent(getMap(t,index),d);
    GMlib::DVector<GMlib::Vector<float,3> > ec2= c2-> evaluateParent(getMap(t,index+1),d);


    this->_p[0] = ec1[0]+ Bfunc[0]*(ec2[0] - ec1[0]);

    //_p[0] -= this->translate();

    if( d > 0)
        this->_p[1] = ec1[1]+ Bfunc[0]*( ec2[1] - ec1[1])
            + Bfunc[1]*( ec2[0] -  ec1[0]) ;

    if( d > 1)
        this->_p[2] = ec1[0]+ Bfunc[0]*( ec2[2] -  ec1[2])
            + 2*Bfunc[1]*( ec2[1] -  ec1[1])
            + Bfunc[2]*(ec2[0] -  ec1[0]) ;
}


template<typename T>
GMlib::DVector<float> ebrsCurve<T>::B(float t, int d){
    //B function of order 1 3tsq-2tcube
    GMlib::DVector<float> b(d+1);

    b[0]= 3*pow(t,2)-2*pow(t,3);
    if(d>0)
        b[1]= 6*t-6*pow(t,2);// first derivative
    if(d>1)
        b[2]= 6-12*t;// 2nd  derivative
    if(d>2)
        b[3]= -12;// 3rd derivative

    return(b);
}

template<typename T>
T ebrsCurve<T>::getEndP() {
    return kvector[kvector.getDim()-2];
}

template<typename T>
T ebrsCurve<T>::getStartP() {
    return kvector[1];
}

template<typename T>
bool ebrsCurve<T>::isClosed() const{
    return isclosed;
}

template<typename T>
void ebrsCurve<T>::localSimulate(double dt){

//    for(int i =0; i< localcurve.getDim()-1;i++){
//    localcurve[i]->rotate(dt,GMlib::Vector<float,3>(1,1,1));// number of curve
//    this->replot(1001,1);

//}

}



