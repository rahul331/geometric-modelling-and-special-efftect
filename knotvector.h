#ifndef KNOTVECTOR_H
#define KNOTVECTOR_H
#include <parametrics/gmpcurve>




class knotvector : public GMlib::DVector<float>
{

public:
   knotvector(int n, float start,float end);

   float getStart();
   float getEnd();

private:


};

#endif // KNOTVECTOR_H
