# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/beziercurve.cpp" "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/beziercurve.cpp.obj"
  "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/controller.cpp" "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/controller.cpp.obj"
  "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/glcontextsurfacewrapper.cpp" "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/glcontextsurfacewrapper.cpp.obj"
  "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/glscenerenderer.cpp" "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/glscenerenderer.cpp.obj"
  "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/gmlibwrapper.cpp" "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/gmlibwrapper.cpp.obj"
  "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/guiapplication.cpp" "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/guiapplication.cpp.obj"
  "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/knotvector.cpp" "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/knotvector.cpp.obj"
  "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/main.cpp" "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/main.cpp.obj"
  "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/moc_curve.cpp" "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/moc_curve.cpp.obj"
  "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/moc_glscenerenderer.cpp" "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/moc_glscenerenderer.cpp.obj"
  "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/moc_gmlibwrapper.cpp" "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/moc_gmlibwrapper.cpp.obj"
  "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/moc_guiapplication.cpp" "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/moc_guiapplication.cpp.obj"
  "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/moc_window.cpp" "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/moc_window.cpp.obj"
  "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/qrc_qml.cpp" "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/qrc_qml.cpp.obj"
  "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/window.cpp" "C:/rahul/STE6247Geometiceffect/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/window.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "GLEW_STATIC"
  "GM_STREAM"
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NETWORK_LIB"
  "QT_QML_LIB"
  "QT_QUICK_LIB"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "C:/rahul/STE6247Geometiceffect/gmlib/BUILD/include"
  "C:/rahul/glew/mingw 4.9.1/release/include"
  "E:/QT/5.4/mingw491_32/include"
  "E:/QT/5.4/mingw491_32/include/QtCore"
  "E:/QT/5.4/mingw491_32/mkspecs/win32-g++"
  "E:/QT/5.4/mingw491_32/include/QtQuick"
  "E:/QT/5.4/mingw491_32/include/QtGui"
  "E:/QT/5.4/mingw491_32/include/QtQml"
  "E:/QT/5.4/mingw491_32/include/QtNetwork"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
