#include "curve.h"
#include"cmath"
#include "iostream"


namespace GMlib{

template<typename T>
Bcurve<T>::Bcurve(T size)
{
    _size = size;
    this->_dm = GMlib::GM_DERIVATION_EXPLICIT;

}

template <typename T>
inline
Bcurve<T>::Bcurve( const Bcurve<T>& copy ) : GMlib::PCurve<T,3>( copy ) {}


template <typename T>
Bcurve<T>::~Bcurve() {}


template <typename T>
inline
void Bcurve<T>::eval( T t, int d, bool /*l*/ ) {

  this->_p.setDim( d + 1 );


    this->_p[0][0] = _size * T( 2* (cos(2*M_PI*t)));
    this->_p[0][1] = _size * T( sin(4*M_PI*t));
    this->_p[0][2] = float(0);

    if(d>0)
    {
        this->_p[1][0] = _size * T( -4*M_PI* (sin(2*M_PI*t)));
        this->_p[1][1] = _size * T( 4*M_PI*cos(4*M_PI*t));
        this->_p[1][2] = float(0);
    }
    if(d>1)
    {
        this->_p[2][0] = _size * T( -8*(std::pow(2,(M_PI)))* cos(2*M_PI*t));
        this->_p[2][1] = _size * T( -16*(std::pow(2,(M_PI)))*sin(4*M_PI*t));
        this->_p[2][2] = float(0);
    }
    if(d>2)
    {
        this->_p[3][0] = _size * T( 16*(std::pow(3,(M_PI)))* sin(2*M_PI*t));
        this->_p[3][1] = _size * T( -64*(std::pow(3,(M_PI)))*cos(4*M_PI*t));
        this->_p[3][2] = float(0);
    }
}


template <typename T>
inline
T Bcurve<T>::getEndP() {
    return T( 5.0 );
}


template <typename T>
inline
T Bcurve<T>::getStartP() {
    return T(0.0);
}


template <typename T>
inline
bool Bcurve<T>::isClosed() const {
    return (true);
}
}




