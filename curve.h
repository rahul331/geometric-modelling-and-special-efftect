#ifndef CURVE_H
#define CURVE_H
#include <parametrics/gmpcurve>



namespace GMlib{

template <typename T>
class Bcurve: public GMlib::PCurve<T,3>{

   GM_SCENEOBJECT(Bcurve)

public:

       Bcurve( T size = T(5) );
       Bcurve( const Bcurve<T>& copy );
       virtual ~Bcurve();

       bool          isClosed() const;


     protected:
       T             _size;
       void	        eval(T t, int d, bool l);
       T             getEndP();
       T             getStartP();
};

}

#include "curve.c"

#endif // CURVE_H
