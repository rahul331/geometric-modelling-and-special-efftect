#ifndef CONTROLLER_H
#define CONTROLLER_H

//#include "parametrics/gmpbeziersurf"
//#include <parametrics/gmpcurve>
//#include"parametrics/gmpbutterfly"

#include <gmSceneModule>
#include <gmParametricsModule>
#include <core/gmarray>
#include <core/gmdmatrix>


#include "curve.h"
#include "knotvector.h"
#include "mysubcurve.h"
#include "ebrscurve.h"
#include"beziercurve.h"
//#include <parametrics/gmpcurve>





class Controllermatrix:public GMlib::SceneObject{//set of surfaces
    GM_SCENEOBJECT(Controllermatrix)

public:

    Controllermatrix(GMlib::Scene  *s);
    void insertScene();

private:

    GMlib::Scene *s;
    GMlib::Bcurve<float>* curve1;
    Beziercurve*          bez;
    mySubCurve<float> *   subCurve;
    ebrsCurve<float>*     ebrs;
    ebrsCurve<float>*     ebrs1;
    ebrsCurve<float>*     ebrs2;

   float                update;
   bool                 moveRight;


protected:
   //void localSimulate(double dt);


};

#endif // CONTROLLER_H
